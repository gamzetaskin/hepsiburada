import com.thoughtworks.gauge.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.sql.DriverManager.println;


public class LoginPage {

    public WebDriver driver;


    @Step("Hepsi Burada anasayfasini ac")
    public void implementation1() {
        System.setProperty("webdriver.chrome.driver", "src/data/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("https://www.hepsiburada.com/");
        // Assert.assertEquals("Türkiye'nin En Büyük Online Alışveriş Sitesi Hepsiburada.com",driver.getTitle());
        System.out.println("Implementation1 succesfully*.");

    }

    @Step("Giris butonuna tikla")
    public void implementation2() throws InterruptedException {
        Thread.sleep(2000);
        //driver.findElement(By.id("shoppingCart")).click();
        driver.findElement(By.xpath("//*[@id=\"myAccount\"]/span/span[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
        // Assert.assertEquals("Giriş Yap", driver.getTitle());
        //driver.findElement(By.id("login")).click();
        System.out.println("Implementation2 succesfully*.");


    }

    @Step("Kullanici bilgilerini doldur")
    public void implementation3() throws InterruptedException {

        Thread.sleep(2000);
        driver.findElement(By.id("txtUserName")).sendKeys("gamzeetskn@gmail.com"); //giris bilgilerinin doldurulması.
        driver.findElement(By.id("txtPassword")).sendKeys("Metallica78.");

       driver.findElement(By.id("btnLogin")).click(); //Giris yap butonuna tıklanılmasının  test edilmesi.
        System.out.println("Implementation3 succesfully*.");

    }

    @Step("Kullanici bilgilerini teyit et")
    public void implementation4() throws InterruptedException {

        String username="Gamze Taskin";
        Thread.sleep(2000);
        String name=driver.findElement(By.xpath("//*[@id=\"myAccount\"]/span/a/span[2]")).getText();
        if(name.equals(username))
        {
            println("İsim kontrolü saglanildi.");
        }
        else
        {
            println("Kontrol saglanamadi");
        }

        System.out.println("Implementation4 succesfully*.");

    }
    @Step("Hepsi Burada sitesini kapatılması")
    public void implementation5() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
        System.out.println("Implementation5 succesfully*.");

    }
}

